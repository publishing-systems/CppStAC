/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Field.cpp
 * @author Stephan Kreutzer
 * @since 2020-04-05
 */

#include "Field.h"

namespace cppstac
{

Field::Field(const std::string& strData, const unsigned long& nLineNumber, const unsigned int& nFieldNumber):
  m_strData(strData),
  m_nLineNumber(nLineNumber),
  m_nFieldNumber(nFieldNumber)
{
    /** @todo Consider a boolean bHasNext parameter. */
}

const std::string& Field::GetData()
{
    return m_strData;
}

const unsigned long& Field::GetLineNumber()
{
    return m_nLineNumber;
}

const unsigned int& Field::GetFieldNumber()
{
    return m_nFieldNumber;
}

}
