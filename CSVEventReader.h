/* Copyright (C) 2017-2020 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CSVEventReader.h
 * @author Stephan Kreutzer
 * @since 2020-03-22
 */

#ifndef _CPPSTAC_CSVEVENTREADER_H
#define _CPPSTAC_CSVEVENTREADER_H

#include "CSVEvent.h"
#include <istream>
#include <string>
#include <memory>
#include <queue>

namespace cppstac
{

/**
 * @todo Might not support Unicode encoding, as it's based on
 *     reading chars.
 */
class CSVEventReader
{
public:
    CSVEventReader(std::istream& aStream, const std::string& strDelimiter, bool bIgnoreFirstLine);
    ~CSVEventReader();

    bool hasNext();
    std::unique_ptr<CSVEvent> nextEvent();

protected:
    static int RETURN_HANDLELINE_ENDOFFIELD;
    static int RETURN_HANDLELINE_ENDOFLINE;
    static int RETURN_HANDLELINE_OUTOFCHARACTERS;
    static int RETURN_HANDLELINE_NOCHARACTERS;
    int HandleLine(bool bOmitOutput);

    static int RETURN_HANDLEFIELD_ENDOFFIELD;
    static int RETURN_HANDLEFIELD_ENDOFLINE;
    static int RETURN_HANDLEFIELD_OUTOFCHARACTERS;
    static int RETURN_HANDLEFIELD_NOCHARACTERS;
    int HandleField(bool bOmitOutput);
    int HandleFieldEnclosed(bool bOmitOmit);

protected:
    std::istream& m_aStream;

    std::string m_strDelimiter = ",";
    bool m_bIgnoreFirstLine = false;

    unsigned int m_nCountDelimiterMatch = 0U;
    unsigned long m_nCountLine = 0UL;
    unsigned int m_nCountFieldMax = 0U;
    unsigned int m_nCountField = 0U;

    unsigned long m_nCountCharacter = 0UL;

    bool m_bHasNextCalled = false;
    std::queue<std::unique_ptr<CSVEvent>> m_aEvents;

};

}

#endif
