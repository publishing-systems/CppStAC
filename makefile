# Copyright (C) 2017-2020  Stephan Kreutzer
#
# This file is part of CppStAC.
#
# CppStAC is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# CppStAC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with CppStAC. If not, see <http://www.gnu.org/licenses/>.



.PHONY: build clean



CFLAGS = -std=c++11 -Wall -Werror -Wextra -pedantic



build: cppstac



cppstac: cppstac.cpp CSVStreamingException.o CSVInputFactory.o CSVEventReader.o CSVEvent.o Field.o EndLine.o StartLine.o
	g++ cppstac.cpp StartLine.o EndLine.o Field.o CSVEvent.o CSVEventReader.o CSVInputFactory.o CSVStreamingException.o -o cppstac $(CFLAGS)

CSVStreamingException.o: CSVStreamingException.h CSVStreamingException.cpp
	g++ CSVStreamingException.cpp -c $(CFLAGS)

CSVInputFactory.o: CSVInputFactory.h CSVInputFactory.cpp
	g++ CSVInputFactory.cpp -c $(CFLAGS)

CSVEventReader.o: CSVEventReader.h CSVEventReader.cpp
	g++ CSVEventReader.cpp -c $(CFLAGS)

CSVEvent.o: CSVEvent.h CSVEvent.cpp
	g++ CSVEvent.cpp -c $(CFLAGS)

StartLine.o: StartLine.h StartLine.cpp
	g++ StartLine.cpp -c $(CFLAGS)

EndLine.o: EndLine.h EndLine.cpp
	g++ EndLine.cpp -c $(CFLAGS)

Field.o: Field.h Field.cpp
	g++ Field.cpp -c $(CFLAGS)

clean:
	rm -f ./cppstac
	rm -f ./cppstac.o
	rm -f ./CSVStreamingException.o
	rm -f ./CSVInputFactory.o
	rm -f ./CSVEventReader.o
	rm -f ./CSVEvent.o
	rm -f ./StartLine.o
	rm -f ./EndLine.o
	rm -f ./Field.o
