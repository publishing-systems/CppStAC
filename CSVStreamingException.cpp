/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CSVStreamingException.cpp
 * @author Stephan Kreutzer
 * @since 2020-04-04
 */

#include "CSVStreamingException.h"

namespace cppstac
{

CSVStreamingException::CSVStreamingException(const std::string& strMessage):
  std::runtime_error(strMessage)
{

}

}
