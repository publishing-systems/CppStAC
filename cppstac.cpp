/* Copyright (C) 2017-2023 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/cppstac.cpp
 * @brief Demo program.
 * @author Stephan Kreutzer
 * @since 2020-03-22
 */

#include "CSVInputFactory.h"
#include <memory>
#include <iostream>
#include <fstream>

typedef std::unique_ptr<cppstac::CSVInputFactory> CSVInputFactory;
typedef std::unique_ptr<cppstac::CSVEventReader> CSVEventReader;
typedef std::unique_ptr<cppstac::CSVEvent> CSVEvent;



int Run(std::istream& aStream);



int main(int argc, char* argv[])
{
    std::cout << "CppStAC Copyright (C) 2017-2023 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repository\n"
              << "https://gitlab.com/publishing-systems/CppStAC/ and\n"
              << "the project website https://publishing-systems.org.\n"
              << std::endl;

    std::unique_ptr<std::ifstream> pStream = nullptr;

    try
    {
        if (argc >= 2)
        {
            pStream = std::unique_ptr<std::ifstream>(new std::ifstream);
            pStream->open(argv[1], std::ios::in | std::ios::binary);

            if (pStream->is_open() != true)
            {
                std::cout << "Couldn't open input file '" << argv[1] << "'.";
                return -1;
            }

            Run(*pStream);

            pStream->close();
        }
        else
        {
            std::cout << "Please enter your CSV input data, confirm with Ctrl+D:" << std::endl;
            Run(std::cin);
        }
    }
    catch (std::exception* pException)
    {
        std::cout << "Exception: " << pException->what() << std::endl;

        if (pStream != nullptr)
        {
            if (pStream->is_open() == true)
            {
                pStream->close();
            }
        }

        return -1;
    }

    return 0;
}

int Run(std::istream& aStream)
{
    CSVInputFactory pFactory = cppstac::CSVInputFactory::newInstance();
    CSVEventReader pReader = pFactory->createCSVEventReader(aStream);

    // Instead of looking at CSVEvents sequentially, one could
    // also implement a "parse tree" to react to CSVEvents, so
    // writing state machines can be avoided because of the
    // implicit call tree context, pretty much like CppStAC
    // does it's parsing.
    while (pReader->hasNext() == true)
    {
        CSVEvent pEvent = pReader->nextEvent();

        if (pEvent->isStartLine() == true)
        {
            //cppstac::StartLine& aStartLine = pEvent->asStartLine();
        }
        else if (pEvent->isField() == true)
        {
            cppstac::Field aField = pEvent->asField();

            if (aField.GetFieldNumber() > 1U)
            {
                std::cout << ",";
            }

            std::cout << "\"";

            for (std::string::const_iterator iter = aField.GetData().begin();
                iter != aField.GetData().end();
                iter++)
            {
                switch (*iter)
                {
                case '\"':
                    std::cout << "\"\"";
                    break;
                default:
                    std::cout << *iter;
                    break;
                }
            }

            std::cout << "\"";
        }
        else if (pEvent->isEndLine() == true)
        {
            //cppstac::EndLine aEndLine = pEvent->asEndLine();

            std::cout << "\r\n";
        }
        else
        {

        }
    }

    return 0;
}
