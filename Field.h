/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Field.h
 * @author Stephan Kreutzer
 * @since 2020-04-05
 */

#ifndef _CPPSTAC_FIELD_H
#define _CPPSTAC_FIELD_H

#include <string>

namespace cppstac
{

class Field
{
public:
    Field(const std::string& strData, const unsigned long& nLineNumber, const unsigned int& nFieldNumber);

    const std::string& GetData();
    const unsigned long& GetLineNumber();
    const unsigned int& GetFieldNumber();

protected:
    std::string m_strData;
    unsigned long m_nLineNumber;
    unsigned int m_nFieldNumber;

};

}

#endif
