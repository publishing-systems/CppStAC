/* Copyright (C) 2017-2023 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CSVEventReader.cpp
 * @author Stephan Kreutzer
 * @since 2020-03-22
  */

#include "CSVEventReader.h"
#include "CSVStreamingException.h"
#include "StartLine.h"
#include "EndLine.h"
#include "Field.h"
#include <sstream>
#include <memory>
#include <iomanip>

namespace cppstac
{

CSVEventReader::CSVEventReader(std::istream& aStream, const std::string& strDelimiter, bool bIgnoreFirstLine):
  m_aStream(aStream),
  m_strDelimiter(strDelimiter),
  m_bIgnoreFirstLine(bIgnoreFirstLine),
  m_bHasNextCalled(false)
{

}

CSVEventReader::~CSVEventReader()
{

}

bool CSVEventReader::hasNext()
{
    if (m_aEvents.size() > 0)
    {
        return true;
    }

    if (m_bHasNextCalled == true)
    {
        return false;
    }
    else
    {
        m_bHasNextCalled = true;
    }


    if (m_bIgnoreFirstLine == true)
    {
        while (true)
        {
            int nResult = HandleLine(m_bIgnoreFirstLine);

            if (nResult == CSVEventReader::RETURN_HANDLELINE_ENDOFFIELD)
            {
                continue;
            }
            else if (nResult == CSVEventReader::RETURN_HANDLELINE_ENDOFLINE)
            {
                // Successfully omitted.
                break;
            }
            else if (nResult == CSVEventReader::RETURN_HANDLELINE_OUTOFCHARACTERS)
            {
                return false;
            }
            else if (nResult == CSVEventReader::RETURN_HANDLELINE_NOCHARACTERS)
            {
                return false;
            }
            else
            {
                throw new std::runtime_error("Unsupported operation.");
            }
        }

        m_bIgnoreFirstLine = false;
    }

    while (true)
    {
        int nResult = HandleLine(false);

        if (nResult == CSVEventReader::RETURN_HANDLELINE_ENDOFFIELD)
        {
            break;
        }
        else if (nResult == CSVEventReader::RETURN_HANDLELINE_ENDOFLINE)
        {
            break;
        }
        else if (nResult == CSVEventReader::RETURN_HANDLELINE_OUTOFCHARACTERS)
        {
            break;
        }
        else if (nResult == CSVEventReader::RETURN_HANDLELINE_NOCHARACTERS)
        {
            break;
        }
        else
        {
            throw new std::runtime_error("Unsupported operation.");
        }
    }

    return m_aEvents.size() > 0;
}

int CSVEventReader::RETURN_HANDLELINE_ENDOFFIELD = 0;
int CSVEventReader::RETURN_HANDLELINE_ENDOFLINE = 1;
int CSVEventReader::RETURN_HANDLELINE_OUTOFCHARACTERS = 2;
int CSVEventReader::RETURN_HANDLELINE_NOCHARACTERS = 3;

int CSVEventReader::HandleLine(bool bOmitOutput)
{
    m_nCountDelimiterMatch = 0U;
    m_nCountCharacter = 0UL;

    while (true)
    {
        int nResult = HandleField(bOmitOutput);

        if (nResult == CSVEventReader::RETURN_HANDLEFIELD_ENDOFFIELD)
        {
            return CSVEventReader::RETURN_HANDLELINE_ENDOFFIELD;
        }
        else if (nResult == CSVEventReader::RETURN_HANDLEFIELD_ENDOFLINE ||
                 nResult == CSVEventReader::RETURN_HANDLEFIELD_OUTOFCHARACTERS)
        {
            if (m_nCountFieldMax == 0U)
            {
                m_nCountFieldMax = m_nCountField;
            }
            else
            {
                if (m_nCountField != m_nCountFieldMax)
                {
                    std::stringstream strMessage;
                    strMessage << "Irregular field count " << m_nCountField << " while a count of " << m_nCountFieldMax << " was expected on line " << (m_nCountLine + 1UL) << ".";

                    throw new CSVStreamingException(strMessage.str());
                }
            }

            m_nCountField = 0U;

            if (bOmitOutput == false)
            {
                std::unique_ptr<EndLine> pEndLine(new EndLine(m_nCountLine + 1UL));
                std::unique_ptr<CSVEvent> pEvent(new CSVEvent(nullptr,
                                                              std::move(pEndLine),
                                                              nullptr));
                m_aEvents.push(std::move(pEvent));
            }

            ++m_nCountLine;

            if (nResult == CSVEventReader::RETURN_HANDLEFIELD_ENDOFLINE)
            {
                return CSVEventReader::RETURN_HANDLELINE_ENDOFLINE;
            }
            else if (nResult == CSVEventReader::RETURN_HANDLEFIELD_OUTOFCHARACTERS)
            {
                return CSVEventReader::RETURN_HANDLELINE_OUTOFCHARACTERS;
            }
            else
            {
                throw new std::runtime_error("Unsupported operation.");
            }
        }
        else if (nResult == CSVEventReader::RETURN_HANDLEFIELD_NOCHARACTERS)
        {
            if (m_nCountFieldMax == 0U)
            {
                // First line and EOF as the first field.
                return CSVEventReader::RETURN_HANDLELINE_NOCHARACTERS;
            }
            else
            {
                if (m_nCountField == 0U)
                {
                    // CRLF at the end of previous line, then EOF
                    // as the first field, valid according to the
                    // RFC and shouldn't lead to an exception because
                    // of irregularity, shouldn't lead to a StartLine
                    // with a first empty field in it.
                    return CSVEventReader::RETURN_HANDLELINE_NOCHARACTERS;
                }
                else
                {
                    // Should never happen, CSVEventReader::RETURN_HANDLEFIELD_NOCHARACTERS
                    // did check for m_nCountField == 0U.
                    throw new std::runtime_error("Unsupported operation.");
                }
            }
        }
        else
        {
            throw new std::runtime_error("Unsupported operation.");
        }
    }
}

int CSVEventReader::RETURN_HANDLEFIELD_ENDOFFIELD = 0;
int CSVEventReader::RETURN_HANDLEFIELD_ENDOFLINE = 1;
int CSVEventReader::RETURN_HANDLEFIELD_OUTOFCHARACTERS = 2;
int CSVEventReader::RETURN_HANDLEFIELD_NOCHARACTERS = 3;

int CSVEventReader::HandleField(bool bOmitOutput)
{
    m_nCountDelimiterMatch = 0U;

    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true &&
        m_nCountField == 0U)
    {
        return RETURN_HANDLEFIELD_NOCHARACTERS;
    }

    if (m_aStream.bad() == true)
    {
        throw new CSVStreamingException("Stream is bad.");
    }

    m_nCountCharacter = 0UL;

    if (m_nCountField == 0U &&
        bOmitOutput == false)
    {
        std::unique_ptr<StartLine> pStartLine(new StartLine(m_nCountLine + 1UL));
        std::unique_ptr<CSVEvent> pEvent(new CSVEvent(std::move(pStartLine),
                                                      nullptr,
                                                      nullptr));
        m_aEvents.push(std::move(pEvent));
    }

    std::stringstream strBuffer;

    while (true)
    {
        ++m_nCountCharacter;

        if (cByte == '\r')
        {
            if (m_nCountDelimiterMatch > 0U)
            {
                if (bOmitOutput == false)
                {
                    strBuffer << m_strDelimiter.substr(0, m_nCountCharacter);
                }

                m_nCountDelimiterMatch = 0U;
            }

            m_aStream.get(cByte);

            if (m_aStream.eof() == true)
            {
                return CSVEventReader::RETURN_HANDLEFIELD_NOCHARACTERS;
            }

            if (m_aStream.bad() == true)
            {
                throw new CSVStreamingException("Stream is bad.");
            }

            if (cByte == '\n')
            {
                if (bOmitOutput == false)
                {
                    std::unique_ptr<Field> pField(new Field(strBuffer.str(), m_nCountLine + 1UL, m_nCountField + 1U));
                    std::unique_ptr<CSVEvent> pEvent(new CSVEvent(nullptr,
                                                                  nullptr,
                                                                  std::move(pField)));
                    m_aEvents.push(std::move(pEvent));
                }

                ++m_nCountField;

                return CSVEventReader::RETURN_HANDLEFIELD_ENDOFLINE;
            }
            else
            {
                if (bOmitOutput == false)
                {
                    strBuffer << '\r';
                }

                continue;
            }
        }
        else if (cByte == m_strDelimiter.at(m_nCountDelimiterMatch))
        {
            ++m_nCountDelimiterMatch;

            if (m_nCountDelimiterMatch == m_strDelimiter.length())
            {
                if (bOmitOutput == false)
                {
                    std::unique_ptr<Field> pField(new Field(strBuffer.str(), m_nCountLine + 1UL, m_nCountField + 1U));
                    std::unique_ptr<CSVEvent> pEvent(new CSVEvent(nullptr,
                                                                  nullptr,
                                                                  std::move(pField)));
                    m_aEvents.push(std::move(pEvent));
                }

                m_nCountDelimiterMatch = 0U;
                ++m_nCountField;

                return CSVEventReader::RETURN_HANDLEFIELD_ENDOFFIELD;
            }
        }
        else if (cByte == '"')
        {
            if (m_nCountCharacter == 1UL)
            {
                return HandleFieldEnclosed(bOmitOutput);
            }
            else
            {
                if (m_nCountDelimiterMatch > 0U)
                {
                    if (bOmitOutput == false)
                    {
                        strBuffer << m_strDelimiter.substr(0, m_nCountDelimiterMatch);
                    }

                    m_nCountDelimiterMatch = 0U;
                }

                if (bOmitOutput == false)
                {
                    strBuffer << cByte;
                }
            }
        }
        else
        {
            if (m_nCountDelimiterMatch > 0U)
            {
                if (bOmitOutput == false)
                {
                    strBuffer << m_strDelimiter.substr(0, m_nCountDelimiterMatch);
                }

                m_nCountDelimiterMatch = 0U;
            }

            if (bOmitOutput == false)
            {
                strBuffer << cByte;
            }
        }

        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            break;
        }

        if (m_aStream.bad() == true)
        {
            throw new CSVStreamingException("Stream is bad.");
        }
    }

    return CSVEventReader::RETURN_HANDLEFIELD_OUTOFCHARACTERS;
}

int CSVEventReader::HandleFieldEnclosed(bool bOmitOutput)
{
    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        std::stringstream strMessage;
        strMessage << "Unexpected end of stream while trying to parse an enclosed field on line " << (m_nCountLine + 1UL) << ".";

        throw new CSVStreamingException(strMessage.str());
    }

    if (m_aStream.bad() == true)
    {
        throw new CSVStreamingException("Stream is bad.");
    }

    std::stringstream strBuffer;

    while (true)
    {
        ++m_nCountCharacter;

        if (cByte == '"')
        {
            m_aStream.get(cByte);

            if (m_aStream.eof() == true)
            {
                std::stringstream strMessage;
                strMessage << "Unexpected end of stream while trying to parse an enclosed field on line " << (m_nCountLine + 1UL) << ".";

                throw new CSVStreamingException(strMessage.str());
            }

            if (m_aStream.bad() == true)
            {
                throw new CSVStreamingException("Stream is bad.");
            }

            ++m_nCountCharacter;

            if (cByte == '"')
            {
                if (bOmitOutput == false)
                {
                    strBuffer << cByte;
                }
            }
            else
            {
                if (bOmitOutput == false)
                {
                    std::unique_ptr<Field> pField(new Field(strBuffer.str(), m_nCountLine + 1UL, m_nCountField + 1U));
                    std::unique_ptr<CSVEvent> pEvent(new CSVEvent(nullptr,
                                                                  nullptr,
                                                                  std::move(pField)));
                    m_aEvents.push(std::move(pEvent));
                }

                while (true)
                {
                    if (cByte == '\r')
                    {
                        if (m_nCountDelimiterMatch > 0U)
                        {
                            std::stringstream strMessage;
                            strMessage << "Carriage return character encountered while matching the field delimiter after the end of an enclosed field on line " << (m_nCountLine + 1UL) << ".";

                            throw new CSVStreamingException(strMessage.str());
                        }

                        m_aStream.get(cByte);

                        if (m_aStream.eof() == true)
                        {
                            std::stringstream strMessage;
                            strMessage << "Unexpected end of stream while trying to parse an enclosed field on line " << (m_nCountLine + 1L) << ".";

                            throw new CSVStreamingException(strMessage.str());
                        }

                        if (m_aStream.bad() == true)
                        {
                            throw new CSVStreamingException("Stream is bad.");
                        }

                        ++m_nCountCharacter;

                        if (cByte == '\n')
                        {
                            ++m_nCountField;

                            return CSVEventReader::RETURN_HANDLEFIELD_ENDOFLINE;
                        }
                        else
                        {
                            std::stringstream strMessage;
                            strMessage << "Carriage return character wasn't followed up by a line feed character while trying to parse an enclosed field on line " << (m_nCountLine + 1UL) << ".";

                            throw new CSVStreamingException(strMessage.str());
                        }
                    }
                    else if (cByte == m_strDelimiter.at(m_nCountDelimiterMatch))
                    {
                        ++m_nCountDelimiterMatch;

                        if (m_nCountDelimiterMatch == m_strDelimiter.length())
                        {
                            m_nCountDelimiterMatch = 0U;
                            ++m_nCountField;

                            return CSVEventReader::RETURN_HANDLEFIELD_ENDOFFIELD;
                        }
                    }
                    else
                    {
                        std::stringstream strMessage;
                        strMessage << "Illegal character '" << cByte << "' (0x"
                                   << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << int(cByte)
                                   << ") after the closing quotation mark of an enclosed field on line " << (m_nCountLine + 1UL) << ".";

                        throw new CSVStreamingException(strMessage.str());
                    }

                    m_aStream.get(cByte);

                    if (m_aStream.eof() == true)
                    {
                        std::stringstream strMessage;
                        strMessage << "Unexpected end of stream while trying to parse an enclosed field on line " << (m_nCountLine + 1L) << ".";

                        throw new CSVStreamingException(strMessage.str());
                    }

                    if (m_aStream.bad() == true)
                    {
                        throw new CSVStreamingException("Stream is bad.");
                    }

                    ++m_nCountCharacter;
                }
            }
        }
        else
        {
            if (bOmitOutput == false)
            {
                strBuffer << cByte;
            }
        }

        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            std::stringstream strMessage;
            strMessage << "Unexpected end of stream while trying to parse an enclosed field on line " << (m_nCountLine + 1UL) << ".";

            throw new CSVStreamingException(strMessage.str());
        }

        if (m_aStream.bad() == true)
        {
            throw new CSVStreamingException("Stream is bad.");
        }
    }
}

std::unique_ptr<CSVEvent> CSVEventReader::nextEvent()
{
    if (m_aEvents.size() <= 0 &&
        m_bHasNextCalled == false)
    {
        if (hasNext() != true)
        {
            throw new std::logic_error("Attempted CSVEventReader::nextEvent() while there isn't one instead of checking CSVEventReader::hasNext() first.");
        }
    }

    m_bHasNextCalled = false;

    if (m_aEvents.size() <= 0)
    {
        throw new std::logic_error("CSVEventReader::nextEvent() while there isn't one, ignoring CSVEventReader::hasNext() == false.");
    }

    std::unique_ptr<CSVEvent> pEvent = std::move(m_aEvents.front());
    m_aEvents.pop();

    return pEvent;
}

}
