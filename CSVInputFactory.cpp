/* Copyright (C) 2017-2020 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CSVInputFactory.cpp
 * @author Stephan Kreutzer
 * @since 2020-03-22
 */

#include "CSVInputFactory.h"
#include <stdexcept>

namespace cppstac
{

CSVInputFactory::CSVInputFactory()
{

}

std::unique_ptr<CSVInputFactory> CSVInputFactory::newInstance()
{
    return std::move(std::unique_ptr<CSVInputFactory>(new CSVInputFactory()));
}

std::unique_ptr<CSVEventReader> CSVInputFactory::createCSVEventReader(std::istream& aStream)
{
    return std::move(std::unique_ptr<CSVEventReader>(new CSVEventReader(aStream, m_strDelimiter, m_bIgnoreFirstLine)));
}

int CSVInputFactory::setDelimiter(const std::string& strDelimiter)
{
    if (strDelimiter.empty() == true)
    {
        throw new std::invalid_argument("Delimiter is an empty string.");
    }

    if (strDelimiter.find("\"") != std::string::npos ||
        strDelimiter.find("\r") != std::string::npos ||
        strDelimiter.find("\n") != std::string::npos)
    {
        throw new std::invalid_argument("Delimiter contains a reserved CSV character.");
    }

    m_strDelimiter = strDelimiter;
    return 0;
}

int CSVInputFactory::setIgnoreFirstLine(bool bIgnore)
{
    m_bIgnoreFirstLine = bIgnore;
    return 0;
}

}
