/* Copyright (C) 2017-2020 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CSVInputFactory.h
 * @author Stephan Kreutzer
 * @since 2020-03-22
 */


#ifndef _CPPSTAC_CSVINPUTFACTORY_H
#define _CPPSTAC_CSVINPUTFACTORY_H

#include "CSVEventReader.h"
#include <istream>
#include <memory>
#include <string>

namespace cppstac
{

class CSVInputFactory
{
protected:
    CSVInputFactory();

public:
    static std::unique_ptr<CSVInputFactory> newInstance();

    std::unique_ptr<CSVEventReader> createCSVEventReader(std::istream& aStream);

    int setDelimiter(const std::string& strDelimiter);
    int setIgnoreFirstLine(bool bIgnore);

protected:
    std::string m_strDelimiter = ",";
    bool m_bIgnoreFirstLine = false;

};

}

#endif
