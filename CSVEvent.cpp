/* Copyright (C) 2017-2023 Stephan Kreutzer
 *
 * This file is part of CppStAC.
 *
 * CppStAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CSVEvent.cpp
 * @author Stephan Kreutzer
 * @since 2020-04-04
 */

#include "CSVEvent.h"
#include <stdexcept>

namespace cppstac
{

CSVEvent::CSVEvent(std::unique_ptr<StartLine> pStartLine,
                   std::unique_ptr<EndLine> pEndLine,
                   std::unique_ptr<Field> pField):
  m_pStartLine(std::move(pStartLine)),
  m_pEndLine(std::move(pEndLine)),
  m_pField(std::move(pField))
{
    int nPointerCount = 0;

    if (m_pStartLine != nullptr)
    {
        ++nPointerCount;
    }

    if (m_pEndLine != nullptr)
    {
        ++nPointerCount;
    }

    if (m_pField != nullptr)
    {
        ++nPointerCount;
    }

    if (nPointerCount != 1)
    {
        throw new std::invalid_argument("CSVEvent constructor expects exactly 1 parameter to be set.");
    }
}

bool CSVEvent::isStartLine()
{
    return m_pStartLine != nullptr;
}

StartLine& CSVEvent::asStartLine()
{
    if (m_pStartLine == nullptr)
    {
        throw new std::logic_error("Isn't a StartLine.");
    }

    return *m_pStartLine;
}

bool CSVEvent::isEndLine()
{
    return m_pEndLine != nullptr;
}

EndLine& CSVEvent::asEndLine()
{
    if (m_pEndLine == nullptr)
    {
        throw new std::logic_error("Isn't an EndLine.");
    }

    return *m_pEndLine;
}

bool CSVEvent::isField()
{
    return m_pField != nullptr;
}

Field& CSVEvent::asField()
{
    if (m_pField == nullptr)
    {
        throw new std::logic_error("Isn't a Field.");
    }

    return *m_pField;
}

}
